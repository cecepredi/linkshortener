$(function () {
    //Advanced form with validation
    var form = $('#wizard_with_validation').show();
    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
            $.AdminBSB.input.activate();

            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
            var host = getHostName($('#url').val());
            var splitHost
            $('#title').val(host);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var url = $('#submit_url').val();
            $.ajax({
                type: "POST",
                url: url,
                data: $('#wizard_with_validation').serialize(),
                dataType: 'json',
                cache: false,
                success: function(result)
                {
                    if (result.error == 0) {
                        swal("Good Job!", result.message, "success")
                        .then((value) => {
                            window.top.location.href= result.url;
                        });
                    } else {
                        swal("Warning", result.message, "error");
                    }
                },
            });
        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'confirm': {
                equalTo: '#password'
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

function getHostName(url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
        var result = match[2];
        var split = result.split(".");
        if (split.length == 2) {
            return split[0].charAt(0).toUpperCase() + split[0].slice(1);
        } else if (split.length == 3) {
            return split[0].charAt(0).toUpperCase() + split[0].slice(1) + ' ' + split[1].charAt(0).toUpperCase() + split[1].slice(1);
        } else {
            return match[2];
        }
    }
    else {
        return null;
    }
}
