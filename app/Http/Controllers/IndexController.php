<?php

namespace App\Http\Controllers;
use App\Link;
use App\Click;
use Hashids;
use Validator;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class IndexController extends Controller
{
    public function index()
    {
        $data['all_links'] = $this->getCountData('all_links');
        $data['new_links'] = $this->getCountData('new_links');
        $data['all_clicks'] = $this->getCountData('all_clicks');
        $data['new_clicks'] = $this->getCountData('new_clicks');
        $data['yesterday_clicks'] = $this->getCountData('yesterday_clicks');
        $data['latest_clicks'] = $this->getTop('latest_clicks');
        $data['top_clicks'] = $this->getTop('top_clicks');
        return view('welcome', $data);
    }

    public function getTop($type='latest_clicks')
    {
        if ($type == 'latest_clicks') {
            return Click::select('links.title', 'links.url_code')
                    ->leftJoin('links', 'clicks.link_id', '=', 'links.id')
                    ->orderBy('clicks.created_at', 'desc')
                    ->orderBy('links.title', 'asc')
                    ->groupBy('clicks.link_id', 'links.url_code','clicks.created_at', 'links.title')
                    ->limit(10)
                    ->get();
        } else {
            return Link::select('links.title', 'links.url_code', DB::raw('count(clicks.id) as total'))
                        ->leftJoin('clicks', 'links.id', '=', 'clicks.link_id')
                        ->orderBy('total', 'desc')
                        ->orderBy('links.title', 'asc')
                        ->groupBy('links.url_code','links.title')
                        ->limit(10)
                        ->get();
        }
        // SELECT department.name, count(employee.id) as count_of_employee from department left join employee on department.id = employee.dept_id group by department.name order by count_of_employee desc, department.name ASC;
    }

    public function getCountData($type='all_links')
    {
        $result = 0;
        switch ($type) {
            case 'all_links':
                $result = Link::count();
                break;
            case 'new_links':
                $result = Link::whereDate('created_at', Carbon::today())->count();
                break;
            case 'all_clicks':
                $result = Click::count();
                break;
            case 'new_clicks':
                $result = Click::whereDate('created_at', Carbon::today())->count();
                break;
            case 'yesterday_clicks':
                $result = Click::whereDate('created_at', Carbon::yesterday())->count();
                break;
        }

        return $result;
    }

    public function getData(Request $request)
    {
        $columns = [
            1 =>'title',
            2=> 'url',
            3=> 'url_code',
            4=> 'action',
        ];

        $limit = $request->length;
        $start = $request->start;
        if ($request->order) {
            $order = $columns[$request->order[0]['column']];
            $dir = $request->order[0]['dir'];
        } else {
            $order = 'updated_at';
            $dir = 'asc';
        }
        $search = $request->search['value'];
        $totalData = 0;

        if (empty($search)) {
            $links = Link::select('title', 'url', 'url_code')
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalData = Link::select('title', 'url', 'url_code')->count();
        } else {
            $links = Link::select('title', 'url', 'url_code')
                        ->where(function($q) use($search) {
                            $q->where(DB::raw("LOWER(title)"),'LIKE',"%{$search}%")
                              ->orWhere(DB::raw("LOWER(url)"),'LIKE',"%{$search}%")
                              ->orWhere(DB::raw("LOWER(url_code)"),'LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalData = Link::select('title', 'url', 'url_code')
                            ->where(function($q) use($search) {
                                $q->where(DB::raw("LOWER(title)"),'LIKE',"%{$search}%")
                                  ->orWhere(DB::raw("LOWER(url)"),'LIKE',"%{$search}%")
                                  ->orWhere(DB::raw("LOWER(url_code)"),'LIKE',"%{$search}%");
                            })
                            ->count();
        }
        $totalFiltered = $totalData;

        $data = [];
        if(!empty($links))
        {
            foreach ($links as $link) {
                $nested['title'] = '<a href="'.route('index').'/'.$link->url_code.'" target="_blank">'.$link->title.'</a>';
                $nested['url'] = '<a href="'.$link->url.'" target="_blank">'.$link->url.'</a>';
                $nested['url_code'] = '<a href="'.route('index').'/'.$link->url_code.'" target="_blank">'.$link->url_code.'</a>';
                $nested['action'] = '';

                $data[] = $nested;
            }
        }

        $json_data = [
                        "draw"            => intval($request->input('draw')),
                        "recordsTotal"    => intval($totalData),
                        "recordsFiltered" => intval($totalFiltered),
                        "data"            => $data
                    ];

        echo json_encode($json_data);
    }

    public function create()
    {
        return view('create');
    }

    public function getCode(Request $request)
    {
        return Hashids::encode((int) date('mdhis'));
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'url' => 'required|url',
            'title' => 'required',
            'url_code' => 'required',
        ]);
        $result = [];

        if ($validation->fails()) {
            $result['error'] = true;
            $result['message'] = 'Please input a valid data.';
        } else {
            $check = Link::where('url_code', $request->url_code)->first();
            if (!$check) {
                $link = new Link;
                $link->title = $request->title;
                $link->url = $request->url;
                $link->url_code = $request->url_code;
                $link->save();
                $result['error'] = false;
                $result['message'] = 'Link successfully created.';
                $result['url'] = route('index');
            } else {
                $result['error'] = true;
                $result['message'] = 'this custom url code has been used. please change to unique values';
            }
        }
        return json_encode($result);
    }

    public function goToLink($code)
    {
        $find = Link::where('url_code', $code)->first();
        if ($find) {
            $ip = $this->ip_address();
            $country = $this->ip_info($ip, 'country');
            $click = new Click;
            $click->link_id = $find->id;
            $click->ip_address = $ip;
            $click->country = $country;
            $click->save();
            return redirect($find->url);
        } else {
            return redirect()->route('index');
        }
    }

    public function ip_address()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE)
    {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }
}
