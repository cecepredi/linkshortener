<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'title', 'url', 'url_code'
    ];

    public function clicks()
    {
       return $this->hasMany(Click::class);
    }
}
