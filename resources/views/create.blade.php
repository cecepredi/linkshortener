@extends('master')
@section('title')
Create
@stop
@section('create_active')
active
@stop
@section('styles')
<!-- Sweet Alert Css -->
<link href="{!! asset('plugins/sweetalert/sweetalert.css') !!}" rel="stylesheet" />
@stop
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Create New Short Link</h2>
            </div>
            <div class="body">
                <form id="wizard_with_validation" method="POST">
                    @csrf
                    <h3>Firt Step</h3>
                    <fieldset>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="url" class="form-control" name="url" id="url" required autocomplete="off">
                                <label class="form-label">Long Url*</label>
                            </div>
                        </div>
                    </fieldset>

                    <h3>Last Step</h3>
                    <fieldset>
                        <h2 class="card-inside-title">Title*</h2>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="title" id="title" class="form-control" placeholder="Title*" required autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="input-group">
                                <span class="input-group-addon">{{ url('/') }}/</span>
                                <div class="form-line">
                                    <input type="text" name="url_code" id="url_code" class="form-control" placeholder="Custom Url" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <input type="hidden" id="submit_url" value="{!! route('store') !!}">
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<!-- Jquery Validation Plugin Css -->
<script src="{!! asset('plugins/jquery-validation/jquery.validate.js') !!}"></script>
<!-- JQuery Steps Plugin Js -->
<script src="{!! asset('plugins/jquery-steps/jquery.steps.js') !!}"></script>
<!-- Sweet Alert Plugin Js -->
<script src="{!! asset('plugins/sweetalert/sweetalert.min.js') !!}"></script>

<script src="{!! asset('js/pages/forms/form-wizard.js') !!}"></script>

<script>
    $(document).ready(function(){
        $.ajax({
            url : "{!! route('getcode') !!}",
            type : "GET",
            success : function(result) {
                $('#url_code').val(result);
            }
        });
    });
</script>
@stop
