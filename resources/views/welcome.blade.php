@extends('master')
@section('title')
Home
@stop
@section('home_active')
active
@stop
@section('styles')
<!-- JQuery DataTable Css -->
<link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@stop
@section('content')
<div class="block-header">
    <h2>Home</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">ALL LINKS</div>
                <div class="number count-to" data-from="0" data-to="{{ $all_links }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">NEW LINKS</div>
                <div class="number count-to" data-from="0" data-to="{{ $new_links }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-light-green hover-expand-effect">
            <div class="icon">
                <i class="material-icons">persons</i>
            </div>
            <div class="content">
                <div class="text">ALL CLICKS</div>
                <div class="number count-to" data-from="0" data-to="{{ $all_links }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-orange hover-expand-effect">
            <div class="icon">
                <i class="material-icons">persons</i>
            </div>
            <div class="content">
                <div class="text">NEW CLICKS</div>
                <div class="number count-to" data-from="0" data-to="{{ $new_clicks }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="body bg-pink">
                <div class="sparkline" data-type="line" data-spot-Radius="4" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#fff"
                     data-min-Spot-Color="rgb(255,255,255)" data-max-Spot-Color="rgb(255,255,255)" data-spot-Color="rgb(255,255,255)"
                     data-offset="90" data-width="100%" data-height="92px" data-line-Width="2" data-line-Color="rgba(255,255,255,0.7)"
                     data-fill-Color="rgba(0, 188, 212, 0)">
                    12,10,9,6,5,6,10,5,7,5,12,13,7,12,11
                </div>
                <ul class="dashboard-stat-list">
                    <li>
                        TODAY
                        <span class="pull-right"><b>{{ $new_clicks }}</b> <small>CLICKS</small></span>
                    </li>
                    <li>
                        YESTERDAY
                        <span class="pull-right"><b>{{ $yesterday_clicks }}</b> <small>CLICKS</small></span>
                    </li>
                    <li>
                        ALL
                        <span class="pull-right"><b>{{ $all_clicks }}</b> <small>CLICKS</small></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="body bg-cyan">
                <div class="m-b--35 font-bold">LATEST CLICKS</div>
                <ul class="dashboard-stat-list">
                    @if (!empty($latest_clicks))
                    @foreach ($latest_clicks as $lat)
                        <li>
                            <a href="{{ route('index') }}/{{ $lat->url_code}}" target="_blank" style="color:white;">#{{$lat->title}}</a>
                            <span class="pull-right">
                                <i class="material-icons">trending_up</i>
                            </span>
                        </li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="body bg-teal">
                <div class="font-bold m-b--35">TOP 10 LINK CLICKS</div>
                <ul class="dashboard-stat-list">
                    @if (!empty($top_clicks))
                    @foreach ($top_clicks as $top)
                        <li>
                            <a href="{{ route('index') }}/{{ $top->url_code}}" target="_blank" style="color:white;">{{$lat->title}}</a>
                            <span class="pull-right"><b>{{ $top->total}}</b> <small>CLICKS</small></span>
                        </li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    List Links
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-list-link dataTable">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Original URL</th>
                                <th>Custom URL</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<!-- Jquery DataTable Plugin Js -->
<script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<!-- Sparkline Chart Plugin Js -->
<script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>
<!-- Jquery CountTo Plugin Js -->
<script src="plugins/jquery-countto/jquery.countTo.js"></script>
<script>
    $(document).ready(function(){
        $('.count-to').countTo();
        initSparkline();
        $('.table-list-link').DataTable({
            responsive: true,
            lengthChange: false,
            processing: true,
            serverSide: true,
    		order: [],
            ajax: "{!! route('getdata') !!}",
            columns: [
                { data: "title" },
                { data: "url" },
                { data: "url_code" },
				{ data: "action", orderable: false, sClass: "text-center" },
            ]
        });
    });

    function initSparkline() {
    $(".sparkline").each(function () {
        var $this = $(this);
        $this.sparkline('html', $this.data());
    });
}
</script>
@stop
