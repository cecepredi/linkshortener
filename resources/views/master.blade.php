<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Link Shortener | @yield('title')</title>
    <!-- Favicon-->
    <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="{!! asset('plugins/bootstrap/css/bootstrap.css') !!}" rel="stylesheet">
    <!-- Waves Effect Css -->
    @yield('styles')
    <link href="{!! asset('plugins/node-waves/waves.css') !!}" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="{!! asset('plugins/animate-css/animate.css') !!}" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{!! asset('css/themes/all-themes.css') !!}" rel="stylesheet" />

  </head>
  <body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">Link Shortener</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{!! asset('images/user.png') !!}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cecep Redi</div>
                    <div class="email">cecepredi@gmail.com</div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="@yield('home_active')">
                        <a href="/">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="@yield('create_active')">
                        <a href="{!! route('create') !!}">
                            <i class="material-icons">edit</i>
                            <span>Create</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2019 <a href="javascript:void(0);">Cecep Redi</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <section class="content">
      <div class="container-fluid">
        @yield('content')
      </div>
    </section>
    <!-- Jquery Core Js -->
    <script src="{!! asset('plugins/jquery/jquery.min.js') !!}"></script>
    <!-- Bootstrap Core Js -->
    <script src="{!! asset('plugins/bootstrap/js/bootstrap.js') !!}"></script>
    <!-- Select Plugin Js -->
    <script src="{!! asset('plugins/bootstrap-select/js/bootstrap-select.js') !!}"></script>
    <!-- Slimscroll Plugin Js -->
    <script src="{!! asset('plugins/jquery-slimscroll/jquery.slimscroll.js') !!}"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="{!! asset('plugins/node-waves/waves.js') !!}"></script>
    @yield('scripts')
    <!-- Custom Js -->
    <script src="{!! asset('js/admin.js') !!}"></script>
    <!-- Demo Js -->
    <script src="{!! asset('js/demo.js') !!}"></script>
  </body>
</html>
