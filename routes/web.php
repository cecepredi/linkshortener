<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
Route::get('/{code}', ['as' => 'gotolink', 'uses' => 'IndexController@goToLink']);
Route::get('/link/create', ['as' => 'create', 'uses' => 'IndexController@create']);
Route::get('/link/get-data', ['as' => 'getdata', 'uses' => 'IndexController@getData']);
Route::get('/link/get-code', ['as' => 'getcode', 'uses' => 'IndexController@getCode']);
Route::post('/link/store', ['as' => 'store', 'uses' => 'IndexController@store']);
