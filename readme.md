# Technical Test of Cecep Redi

## Server Requirements
1. PHP >= 7.1.3
2. BCMath PHP Extension
3. Ctype PHP Extension
4. JSON PHP Extension
5. Mbstring PHP Extension
6. OpenSSL PHP Extension
7. PDO PHP Extension
8. Tokenizer PHP Extension
9. XML PHP Extension
10. PostgreSQL

## Setup

1. clone app
2. npm install
3. composer install
4. create _.env_ file or rename _.env.example_ to _.env_
5. create database in PostgreSQL
6. setup database credential in _.env_
7. php artisan serve
8. open app in [here](http://localhost:8000) or [here](http://127.0.0.1:8000)
